<?php

//Callback page cupcake-factory
function buy_cupcake_factory_form(){
  $form = array();

  //Return all nids of nodes of type "cupcake".
  $nids = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->fields('n', array('type'))
      ->condition('n.type', 'cupcake')
      ->execute()
      ->fetchCol(); // returns an indexed array

  //Now return the node objects.
  $cupcakes = node_load_multiple($nids);

  //Array for the list cupcakes
  $cupcakes_list = Array();
  foreach ($cupcakes as $key => $cupcake) {
    $cupcakes_list[$cupcake->nid] = $cupcake->title;
  }

  //Array for the number cupcakes
  $cupcakes_numbers = Array();
  for ($i=1; $i <= 15; $i++) { 
    $cupcakes_numbers[$i] = $i;
  }

  $form['cupcake'] = array (
    '#title' => t('Choose one Cupcake'),
    '#type' => 'select',
    '#options' => $cupcakes_list,
  );
  $form['number'] = array (
    '#title' => t('How many Cupcakes?'),
    '#type' => 'select',
    '#options' => $cupcakes_numbers,
  );
  $form['submit'] = array (
    '#type' => 'submit',
    '#value' => t('Buy!'),
  );
  return $form;
}

//Submit form /cupcake-factory
function buy_cupcake_factory_form_submit(&$form, &$form_state){
  global $user;
  $cupcake_info = node_load($form_state['values']['cupcake']);
  $entity_type = 'cupcake_sale';
  $entity = entity_create('cupcake_sale', array('type' => 'general'));

  //data
  $sale_user = $user->uid;
  $sale_cupcake = $form_state['values']['cupcake'];
  $sale_count = $form_state['values']['number'];
  $sale_total = $cupcake_info->field_cost['und'][0]['value'] * $form_state['values']['number'];
  $sale_date = date("Y-m-d H:i:s");

  //data entuty
  $entity->field_cupcake_sale_user = array(LANGUAGE_NONE => array(0 => array('target_id' => $sale_user)));
  $entity->field_cupcake_sale_cupcake = array(LANGUAGE_NONE => array(0 => array('target_id' => $sale_cupcake)));
  $entity->field_cupcake_sale_count = array(LANGUAGE_NONE => array(0 => array('value' => $sale_count)));
  $entity->field_cupcake_sale_total = array(LANGUAGE_NONE => array(0 => array('value' => $sale_total)));
  $entity->field_cupcake_sale_date = array(LANGUAGE_NONE => array(0 => array('value' => $sale_date)));
  $entity->save();

  drupal_set_message(t("Your purchase is ready, download your invoice in the link: ") . '<a href ="cupcake-sales-invoice/' . $entity->id . '" target="_blank">Invoice</a>');
}