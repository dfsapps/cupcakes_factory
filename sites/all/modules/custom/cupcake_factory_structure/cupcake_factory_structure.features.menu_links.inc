<?php
/**
 * @file
 * cupcake_factory_structure.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function cupcake_factory_structure_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_all-cupcakes-sales:cupcake-sales-list-all.
  $menu_links['navigation_all-cupcakes-sales:cupcake-sales-list-all'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'cupcake-sales-list-all',
    'router_path' => 'cupcake-sales-list-all',
    'link_title' => 'All Cupcakes Sales',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_all-cupcakes-sales:cupcake-sales-list-all',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: navigation_buy-cupcakes:cupcake-factory.
  $menu_links['navigation_buy-cupcakes:cupcake-factory'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'cupcake-factory',
    'router_path' => 'cupcake-factory',
    'link_title' => 'Buy Cupcakes',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_buy-cupcakes:cupcake-factory',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: navigation_my-cupcakes-purchases:cupcake-sales-list-user.
  $menu_links['navigation_my-cupcakes-purchases:cupcake-sales-list-user'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'cupcake-sales-list-user',
    'router_path' => 'cupcake-sales-list-user',
    'link_title' => 'My Cupcakes Purchases',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_my-cupcakes-purchases:cupcake-sales-list-user',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('All Cupcakes Sales');
  t('Buy Cupcakes');
  t('My Cupcakes Purchases');

  return $menu_links;
}
