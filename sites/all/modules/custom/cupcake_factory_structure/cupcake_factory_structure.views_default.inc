<?php
/**
 * @file
 * cupcake_factory_structure.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cupcake_factory_structure_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'cupcake_sales_list_user';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_cupcake_sale';
  $view->human_name = 'Cupcake Sales List User';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Cupcake Sales List User';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'views_bootstrap_table_plugin_style';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'field_cupcake_sale_user' => 'field_cupcake_sale_user',
    'field_cupcake_sale_cupcake' => 'field_cupcake_sale_cupcake',
    'field_cupcake_sale_count' => 'field_cupcake_sale_count',
    'field_cupcake_sale_total' => 'field_cupcake_sale_total',
    'field_cupcake_sale_date' => 'field_cupcake_sale_date',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cupcake_sale_user' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cupcake_sale_cupcake' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cupcake_sale_count' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cupcake_sale_total' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cupcake_sale_date' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['bootstrap_styles'] = array(
    'striped' => 0,
    'bordered' => 0,
    'hover' => 0,
    'condensed' => 0,
  );
  /* Field: Cupcake Sale: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_cupcake_sale';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Filter criterion: Cupcake Sale: cupcake_sale type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_cupcake_sale';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'general' => 'general',
  );

  /* Display: Cupcake Sales List User */
  $handler = $view->new_display('page', 'Cupcake Sales List User', 'page');
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    2 => '2',
  );
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['id'] = 'field_cupcake_sale_user_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['table'] = 'field_data_field_cupcake_sale_user';
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['field'] = 'field_cupcake_sale_user_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['required'] = TRUE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['id'] = 'field_cupcake_sale_cupcake_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['table'] = 'field_data_field_cupcake_sale_cupcake';
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['field'] = 'field_cupcake_sale_cupcake_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Cupcake Sale: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_cupcake_sale';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Field: Cupcake Sale: Cupcake Sale User */
  $handler->display->display_options['fields']['field_cupcake_sale_user']['id'] = 'field_cupcake_sale_user';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['table'] = 'field_data_field_cupcake_sale_user';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['field'] = 'field_cupcake_sale_user';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['label'] = 'User';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['settings'] = array(
    'link' => 0,
  );
  /* Field: Cupcake Sale: Cupcake Sale Cupcake */
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['id'] = 'field_cupcake_sale_cupcake';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['table'] = 'field_data_field_cupcake_sale_cupcake';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['field'] = 'field_cupcake_sale_cupcake';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['label'] = 'Cupcake';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['settings'] = array(
    'link' => 0,
  );
  /* Field: Content: Cost */
  $handler->display->display_options['fields']['field_cost']['id'] = 'field_cost';
  $handler->display->display_options['fields']['field_cost']['table'] = 'field_data_field_cost';
  $handler->display->display_options['fields']['field_cost']['field'] = 'field_cost';
  $handler->display->display_options['fields']['field_cost']['relationship'] = 'field_cupcake_sale_cupcake_target_id';
  $handler->display->display_options['fields']['field_cost']['label'] = 'Unite Cost';
  $handler->display->display_options['fields']['field_cost']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Cupcake Sale: Cupcake Sale Count */
  $handler->display->display_options['fields']['field_cupcake_sale_count']['id'] = 'field_cupcake_sale_count';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['table'] = 'field_data_field_cupcake_sale_count';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['field'] = 'field_cupcake_sale_count';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['label'] = 'Count';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Cupcake Sale: Cupcake Sale Total */
  $handler->display->display_options['fields']['field_cupcake_sale_total']['id'] = 'field_cupcake_sale_total';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['table'] = 'field_data_field_cupcake_sale_total';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['field'] = 'field_cupcake_sale_total';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['label'] = 'Total';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Cupcake Sale: Cupcake Sale Date */
  $handler->display->display_options['fields']['field_cupcake_sale_date']['id'] = 'field_cupcake_sale_date';
  $handler->display->display_options['fields']['field_cupcake_sale_date']['table'] = 'field_data_field_cupcake_sale_date';
  $handler->display->display_options['fields']['field_cupcake_sale_date']['field'] = 'field_cupcake_sale_date';
  $handler->display->display_options['fields']['field_cupcake_sale_date']['label'] = 'Date';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Invoice';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href ="cupcake-sales-invoice/[id]" target="_blank">Invoice</a>';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Cupcake Sale: Cupcake Sale Date (field_cupcake_sale_date:format) */
  $handler->display->display_options['sorts']['field_cupcake_sale_date_format']['id'] = 'field_cupcake_sale_date_format';
  $handler->display->display_options['sorts']['field_cupcake_sale_date_format']['table'] = 'field_data_field_cupcake_sale_date';
  $handler->display->display_options['sorts']['field_cupcake_sale_date_format']['field'] = 'field_cupcake_sale_date_format';
  $handler->display->display_options['sorts']['field_cupcake_sale_date_format']['order'] = 'DESC';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: User: Uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'users';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['relationship'] = 'field_cupcake_sale_user_target_id';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['path'] = 'cupcake-sales-list-user';

  /* Display: Cupcake Sales List All */
  $handler = $view->new_display('page', 'Cupcake Sales List All', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Cupcake Sales List All';
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
  );
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['id'] = 'field_cupcake_sale_user_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['table'] = 'field_data_field_cupcake_sale_user';
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['field'] = 'field_cupcake_sale_user_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['required'] = TRUE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['id'] = 'field_cupcake_sale_cupcake_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['table'] = 'field_data_field_cupcake_sale_cupcake';
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['field'] = 'field_cupcake_sale_cupcake_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Cupcake Sale: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_cupcake_sale';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Field: Cupcake Sale: Cupcake Sale User */
  $handler->display->display_options['fields']['field_cupcake_sale_user']['id'] = 'field_cupcake_sale_user';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['table'] = 'field_data_field_cupcake_sale_user';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['field'] = 'field_cupcake_sale_user';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['label'] = 'User';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['settings'] = array(
    'link' => 0,
  );
  /* Field: Cupcake Sale: Cupcake Sale Cupcake */
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['id'] = 'field_cupcake_sale_cupcake';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['table'] = 'field_data_field_cupcake_sale_cupcake';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['field'] = 'field_cupcake_sale_cupcake';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['label'] = 'Cupcake';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['settings'] = array(
    'link' => 0,
  );
  /* Field: Content: Cost */
  $handler->display->display_options['fields']['field_cost']['id'] = 'field_cost';
  $handler->display->display_options['fields']['field_cost']['table'] = 'field_data_field_cost';
  $handler->display->display_options['fields']['field_cost']['field'] = 'field_cost';
  $handler->display->display_options['fields']['field_cost']['relationship'] = 'field_cupcake_sale_cupcake_target_id';
  $handler->display->display_options['fields']['field_cost']['label'] = 'Unite Cost';
  $handler->display->display_options['fields']['field_cost']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Cupcake Sale: Cupcake Sale Count */
  $handler->display->display_options['fields']['field_cupcake_sale_count']['id'] = 'field_cupcake_sale_count';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['table'] = 'field_data_field_cupcake_sale_count';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['field'] = 'field_cupcake_sale_count';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['label'] = 'Count';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Cupcake Sale: Cupcake Sale Total */
  $handler->display->display_options['fields']['field_cupcake_sale_total']['id'] = 'field_cupcake_sale_total';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['table'] = 'field_data_field_cupcake_sale_total';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['field'] = 'field_cupcake_sale_total';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['label'] = 'Total';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Cupcake Sale: Cupcake Sale Date */
  $handler->display->display_options['fields']['field_cupcake_sale_date']['id'] = 'field_cupcake_sale_date';
  $handler->display->display_options['fields']['field_cupcake_sale_date']['table'] = 'field_data_field_cupcake_sale_date';
  $handler->display->display_options['fields']['field_cupcake_sale_date']['field'] = 'field_cupcake_sale_date';
  $handler->display->display_options['fields']['field_cupcake_sale_date']['label'] = 'Date';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Invoice';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href ="cupcake-sales-invoice/[id]" target="_blank">Invoice</a>';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Cupcake Sale: Cupcake Sale Date (field_cupcake_sale_date) */
  $handler->display->display_options['sorts']['field_cupcake_sale_date_value']['id'] = 'field_cupcake_sale_date_value';
  $handler->display->display_options['sorts']['field_cupcake_sale_date_value']['table'] = 'field_data_field_cupcake_sale_date';
  $handler->display->display_options['sorts']['field_cupcake_sale_date_value']['field'] = 'field_cupcake_sale_date_value';
  $handler->display->display_options['sorts']['field_cupcake_sale_date_value']['order'] = 'DESC';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Cupcake Sale: cupcake_sale type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_cupcake_sale';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'general' => 'general',
  );
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['relationship'] = 'field_cupcake_sale_user_target_id';
  $handler->display->display_options['filters']['uid']['value'] = '';
  $handler->display->display_options['filters']['uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['label'] = 'Search By User';
  $handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
  $handler->display->display_options['filters']['uid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['path'] = 'cupcake-sales-list-all';

  /* Display: Cupcake Sales Invoice */
  $handler = $view->new_display('page', 'Cupcake Sales Invoice', 'page_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Cupcake Sales Invoice';
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    2 => '2',
  );
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['id'] = 'field_cupcake_sale_user_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['table'] = 'field_data_field_cupcake_sale_user';
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['field'] = 'field_cupcake_sale_user_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_user_target_id']['required'] = TRUE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['id'] = 'field_cupcake_sale_cupcake_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['table'] = 'field_data_field_cupcake_sale_cupcake';
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['field'] = 'field_cupcake_sale_cupcake_target_id';
  $handler->display->display_options['relationships']['field_cupcake_sale_cupcake_target_id']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Cupcake Sale: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_cupcake_sale';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = '';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
  /* Field: Cupcake Sale: Cupcake Sale User */
  $handler->display->display_options['fields']['field_cupcake_sale_user']['id'] = 'field_cupcake_sale_user';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['table'] = 'field_data_field_cupcake_sale_user';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['field'] = 'field_cupcake_sale_user';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['label'] = '';
  $handler->display->display_options['fields']['field_cupcake_sale_user']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_cupcake_sale_user']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cupcake_sale_user']['settings'] = array(
    'link' => 0,
  );
  /* Field: Cupcake Sale: Cupcake Sale Cupcake */
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['id'] = 'field_cupcake_sale_cupcake';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['table'] = 'field_data_field_cupcake_sale_cupcake';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['field'] = 'field_cupcake_sale_cupcake';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['label'] = '';
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cupcake_sale_cupcake']['settings'] = array(
    'link' => 0,
  );
  /* Field: Cupcake Sale: Cupcake Sale Count */
  $handler->display->display_options['fields']['field_cupcake_sale_count']['id'] = 'field_cupcake_sale_count';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['table'] = 'field_data_field_cupcake_sale_count';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['field'] = 'field_cupcake_sale_count';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['label'] = '';
  $handler->display->display_options['fields']['field_cupcake_sale_count']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_cupcake_sale_count']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cupcake_sale_count']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Cupcake Sale: Cupcake Sale Total */
  $handler->display->display_options['fields']['field_cupcake_sale_total']['id'] = 'field_cupcake_sale_total';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['table'] = 'field_data_field_cupcake_sale_total';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['field'] = 'field_cupcake_sale_total';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['label'] = '';
  $handler->display->display_options['fields']['field_cupcake_sale_total']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_cupcake_sale_total']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cupcake_sale_total']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Cupcake Sale: Cupcake Sale Date */
  $handler->display->display_options['fields']['field_cupcake_sale_date']['id'] = 'field_cupcake_sale_date';
  $handler->display->display_options['fields']['field_cupcake_sale_date']['table'] = 'field_data_field_cupcake_sale_date';
  $handler->display->display_options['fields']['field_cupcake_sale_date']['field'] = 'field_cupcake_sale_date';
  $handler->display->display_options['fields']['field_cupcake_sale_date']['label'] = '';
  $handler->display->display_options['fields']['field_cupcake_sale_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_cupcake_sale_date']['element_label_colon'] = FALSE;
  /* Field: Content: Cost */
  $handler->display->display_options['fields']['field_cost']['id'] = 'field_cost';
  $handler->display->display_options['fields']['field_cost']['table'] = 'field_data_field_cost';
  $handler->display->display_options['fields']['field_cost']['field'] = 'field_cost';
  $handler->display->display_options['fields']['field_cost']['relationship'] = 'field_cupcake_sale_cupcake_target_id';
  $handler->display->display_options['fields']['field_cost']['label'] = '';
  $handler->display->display_options['fields']['field_cost']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_cost']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cost']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Invoice Number: [id]
<br>
<br>
Invoice generate to: [field_cupcake_sale_user]
<br>
<br>
Invoice Date: [field_cupcake_sale_date]
<br>
<br>
Purchase Detail:
<br><br>
Cupcake: [field_cupcake_sale_cupcake]<br>
Unit Cost: [field_cost]<br>
Count: [field_cupcake_sale_count]<br>
Total: [field_cupcake_sale_total]';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Cupcake Sale: Id */
  $handler->display->display_options['arguments']['id']['id'] = 'id';
  $handler->display->display_options['arguments']['id']['table'] = 'eck_cupcake_sale';
  $handler->display->display_options['arguments']['id']['field'] = 'id';
  $handler->display->display_options['arguments']['id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['path'] = 'cupcake-sales-invoice/%';
  $export['cupcake_sales_list_user'] = $view;

  $view = new view();
  $view->name = 'cupcakes_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Cupcake List';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Cupcake List';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'full';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'cupcake' => 'cupcake',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['style_options']['optionset'] = 'cupcakes_list';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Cupcake Image */
  $handler->display->display_options['fields']['field_cupcake_image']['id'] = 'field_cupcake_image';
  $handler->display->display_options['fields']['field_cupcake_image']['table'] = 'field_data_field_cupcake_image';
  $handler->display->display_options['fields']['field_cupcake_image']['field'] = 'field_cupcake_image';
  $handler->display->display_options['fields']['field_cupcake_image']['label'] = '';
  $handler->display->display_options['fields']['field_cupcake_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_cupcake_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cupcake_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_cupcake_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Cost */
  $handler->display->display_options['fields']['field_cost']['id'] = 'field_cost';
  $handler->display->display_options['fields']['field_cost']['table'] = 'field_data_field_cost';
  $handler->display->display_options['fields']['field_cost']['field'] = 'field_cost';
  $handler->display->display_options['fields']['field_cost']['label'] = '';
  $handler->display->display_options['fields']['field_cost']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_cost']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cost']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Cupcake Type */
  $handler->display->display_options['fields']['field_cupcake_type']['id'] = 'field_cupcake_type';
  $handler->display->display_options['fields']['field_cupcake_type']['table'] = 'field_data_field_cupcake_type';
  $handler->display->display_options['fields']['field_cupcake_type']['field'] = 'field_cupcake_type';
  $handler->display->display_options['fields']['field_cupcake_type']['label'] = '';
  $handler->display->display_options['fields']['field_cupcake_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_cupcake_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cupcake_type']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Ingredients */
  $handler->display->display_options['fields']['field_ingredients']['id'] = 'field_ingredients';
  $handler->display->display_options['fields']['field_ingredients']['table'] = 'field_data_field_ingredients';
  $handler->display->display_options['fields']['field_ingredients']['field'] = 'field_ingredients';
  $handler->display->display_options['fields']['field_ingredients']['label'] = '';
  $handler->display->display_options['fields']['field_ingredients']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_ingredients']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ingredients']['delta_offset'] = '0';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="cupcake-title">[title]</div>
<div class="cupcake-image">[field_cupcake_image]</div>
<div class="cupcake-body">[body]</div>
<div class="cupcake-cost">Cost: [field_cost]</div>
<div class="cupcake-type">Type: [field_cupcake_type]</div>
<div class="cupcake-ingrdients">Ingredients: [field_ingredients]</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $export['cupcakes_list'] = $view;

  return $export;
}
