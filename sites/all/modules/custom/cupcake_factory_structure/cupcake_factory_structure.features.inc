<?php
/**
 * @file
 * cupcake_factory_structure.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cupcake_factory_structure_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cupcake_factory_structure_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function cupcake_factory_structure_eck_bundle_info() {
  $items = array(
    'cupcake_sale_general' => array(
      'machine_name' => 'cupcake_sale_general',
      'entity_type' => 'cupcake_sale',
      'name' => 'general',
      'label' => 'General',
      'config' => array(
        'managed_properties' => array(),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function cupcake_factory_structure_eck_entity_type_info() {
  $items = array(
    'cupcake_sale' => array(
      'name' => 'cupcake_sale',
      'label' => 'Cupcake Sale',
      'properties' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function cupcake_factory_structure_node_info() {
  $items = array(
    'cupcake' => array(
      'name' => t('Cupcake'),
      'base' => 'node_content',
      'description' => t('Cucpacke Data'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
